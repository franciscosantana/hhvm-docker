FROM debian:jessie
MAINTAINER Francisco Santana <salaniojr@gmail.com>

RUN apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0x5a16e7281be7a449
RUN echo deb http://dl.hhvm.com/debian jessie main | tee /etc/apt/sources.list.d/hhvm.list

RUN apt-get update && apt-get -y install wget build-essential libxml2-dev openssl libssl-dev libcurl4-openssl-dev pkg-config libpcre3 libpcre3-dev hhvm

WORKDIR /opt
RUN mkdir php56
WORKDIR php56
RUN wget -O php56.tar.gz http://php.net/get/php-5.6.13.tar.gz/from/this/mirror
RUN tar xvzf php56.tar.gz
WORKDIR php-5.6.13
RUN ./configure --enable-fpm --with-mysql --enable-mbstring --with-openssl --with-pdo-mysql
RUN make
RUN make install

RUN cp sapi/fpm/php-fpm /usr/local/bin

COPY php-fpm.conf /usr/local/etc/php-fpm.conf
COPY php.ini /usr/local/php/php.ini
COPY run.sh /usr/bin/docker-startup.sh
RUN chmod +x /usr/bin/docker-startup.sh

WORKDIR /opt
RUN mkdir nginx
WORKDIR nginx
RUN wget -O nginx.tar.gz http://nginx.org/download/nginx-1.8.0.tar.gz
RUN tar xvzf nginx.tar.gz
WORKDIR nginx-1.8.0
RUN ./configure
RUN make
RUN make install

COPY nginx.conf /usr/local/nginx/conf/nginx.conf

WORKDIR /

RUN rm /usr/local/nginx/html/index.html
RUN echo "<?php phpinfo(); ?>" >> /usr/local/nginx/html/index.php

RUN mkdir /www
RUN ln -s /usr/local/nginx/html/ www/public

EXPOSE 80

ENTRYPOINT ["/usr/bin/docker-startup.sh"]